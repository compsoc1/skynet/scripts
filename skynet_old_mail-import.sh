# This script is to move all teh old mail into a subfolder and to scribe the user to that folder (so it pops up in their mail client)
# This needs to be run on the mailserver else sendmail and doveadm wont be available

echo "Subject: [Skynet] Old emails restored
Hi all,

All the old emails from Skynet prior to Jan 2023 that were on the server have been restored to this folder.

Brendan." > email.txt


FROM=/skynet_old/mail
TO=/var/vmail/skynet.ie
MAILBOX="skynet_old"


for dir in $FROM/*; 
do 
  username=$(basename "$dir");
  
  if [ -z "$(ls -A $FROM/$username)" ]; then
    rm -rf $FROM/$username
    continue
  fi
  
  # a few usernames are bogus, ignore these
  if  [[ $username == bogus* ]] || [[ $username == BOGUS* ]];
  then
    continue
  fi
  
  # should only deal with folks who got mailbox created, did ye run skynet_old_mailbox-send.sh ?
  if [ ! -d "$TO/$username" ]; then
    echo "Fail: $TO/$username does not exist."
    continue
  fi
  
  # we want to put teh main into a sub mailbox
  TO_USER="$TO/$username/.$MAILBOX"
  
  if [ ! -d "$TO_USER" ]; then
    # if it doesnt exist, create it and do the rest of this script.
    mkdir $TO_USER
  fi 
  
  # from https://askubuntu.com/a/483192
  mv $FROM/$username/{*,.[^.]*,..?*} $TO_USER/
  
  # if empty
  if [ -z "$(ls -A $FROM/$username)" ]; then
     rm -rf $FROM/$username
  fi
  
  # set proper permissions
  chmod -R 600 $TO_USER/
  chmod -R u+X $TO_USER/
  
  # change owner to the mailserver
  chown -R virtualMail:virtualMail $TO_USER/
  
  # subscribe the user to the mailbox show it shows up
  doveadm mailbox subscribe -u "$username@skynet.ie" $MAILBOX
  
  # this pops a new mail in that mailbox
  sendmail "$username+$MAILBOX@skynet.ie" < email.txt
  
  echo "Complete: $username";

done