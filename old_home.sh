#!/usr/bin/env bash

# https://discord.com/channels/689189992417067052/1114882414670516294/1142858186085367919
# Used to make old home and old mail available to users who owned them


# Function to recursively change owner and permissions
change_owner_and_permissions() {
    local dir="$1"
    local username="$2"

    # Change owner of the directory and its contents
    chown -R "$username" "$dir"
}

iter_dirs(){
    local root="$1"

    # Loop through each directory in /home
    for dir in $root/*; do
        if [ -d "$dir" ]; then
            # Extract the directory name without the leading path
            username=$(basename "$dir")

            # handle caps
            username_lower=$(echo "$username" | tr '[:upper:]' '[:lower:]')
            if  [ "$username" != "$username_lower" ]; then
                mv "$root/$username" "$root/$username_lower"
                echo "Changed $username to $username_lower"
                dir="$root/$username_lower"
                username=$username_lower
            fi

            # names must atart with either underscore or letter
            if [[ $username =~ ^[0-9] ]]; then
                mv "$root/$username" "$root/_$username"
                echo "Changed $username to _$username"
                dir="$root/_$username"
                username="_$username"
            fi

            # Set permissions to read-only for the owner and not readable by others
            # folders have to be executable to be able to read their contents
            chmod -R u+rX "$dir"

            # Check if the user exists before changing ownership and permissions
            if id "$username" &>/dev/null; then
                change_owner_and_permissions "$dir" "$username"
                echo "Changed owner and permissions of $dir to $username"
            # else
            #     echo "User $username not found. Leaving $dir owned by root."
            fi
        fi
    done
}

iter_dirs /skynet_old/home
iter_dirs /skynet_old/mail