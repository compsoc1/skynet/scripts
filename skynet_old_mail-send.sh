# This script is to send an email to everyone in order to create their email folder.
# would create it using doveadm but that seems like its not possible.

echo "Subject: Testing, please ignore
Testing please ignore" > email.txt

FROM=/skynet_old/mail
TO=/var/vmail/skynet.ie


for dir in $FROM/*; 
do 
  username=$(basename "$dir");
  
  if  [[ $username == bogus* ]] || [[ $username == BOGUS* ]];
  then
    continue
  fi
  
  if [ ! -d "$TO/$username" ]; then
    sendmail "$username@skynet.ie"  < email.txt
    echo "Mail Sent: $username"
  fi
  
done
