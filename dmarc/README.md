# DMARC

Skynet admins get dmarc reports into a folder in tehir mail account called ``mailman``.  
This compose script pulls mails into a databse and displays them on a local website.

Uses https://github.com/gutmensch/docker-dmarc-report

1. ``cp .env.example .env``  
2. Add yer username and pass to the ``.env`` 
3. ``docker compose up``
4. Go to ``localhost`` in your browser to view it.